// Fill out your copyright notice in the Description page of Project Settings.

#include "ModifyAssetsCommandlet.h"
#include "EditorStaticMeshLibrary.h"
#include "Engine/StaticMesh.h"
#include "Modules/ModuleManager.h"
#include "AssetRegistryModule.h"

int32 UModifyAssetsCommandlet::Main(const FString& Params)
{
	TArray<FString> Tokens;
	TArray<FString> Switches;

	TMap<FString, FString> ParamsMap;
	ParseCommandLine(*Params, Tokens, Switches);

	if (Switches.Contains(TEXT("GenerateLod")))
	{
		if (Tokens.Num() > 0)
		{
			ProcessAssets(Tokens);
		}
	}

	return 0;
}

void UModifyAssetsCommandlet::ProcessAssets(TArray<FString> RootDirectories)
{
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(
		AssetRegistryConstants::ModuleName);

	AssetRegistryModule.Get().SearchAllAssets(true);

	FString ClassName = TEXT("StaticMesh");
	TArray<FAssetData> AssetList;
	AssetRegistryModule.Get().GetAssetsByClass(*ClassName, AssetList, true);

	for (FAssetData AssetData : AssetList)
	{
		for (FString RootDirectory : RootDirectories)
		{
			if (AssetData.ObjectPath.ToString().StartsWith(RootDirectory, ESearchCase::IgnoreCase))
			{
				UObject* AssetInstance = AssetData.GetAsset();

				ModifyLod(AssetInstance);

				SaveAsset(AssetInstance);

				break;
			}
		}
	}
}

void UModifyAssetsCommandlet::ModifyLod(UObject* AssetInstance)
{
	if (UStaticMesh* Mesh = Cast<UStaticMesh>(AssetInstance))
	{
		TArray<FEditorScriptingMeshReductionSettings> ReductionSettings;

		FEditorScriptingMeshReductionSettings Settings;

		//LOD 0
		Settings.PercentTriangles = 1;
		Settings.ScreenSize = 0.9;
		ReductionSettings.Add(Settings);

		//LOD 1
		Settings.PercentTriangles = 0.5;
		Settings.ScreenSize = 0.5;
		ReductionSettings.Add(Settings);

		//LOD 2
		Settings.PercentTriangles = 0.1;
		Settings.ScreenSize = 0.3;
		ReductionSettings.Add(Settings);

		FEditorScriptingMeshReductionOptions Options;
		Options.ReductionSettings = ReductionSettings;

		UEditorStaticMeshLibrary::SetLods(Mesh, Options);

		AssetInstance->MarkPackageDirty();
	}
}

void UModifyAssetsCommandlet::SaveAsset(UObject* AssetInstance)
{
	if (AssetInstance)
	{
		if (UPackage* Package = AssetInstance->GetPackage())
		{
			if (Package->IsDirty())
			{
				FString PackageName = FPackageName::LongPackageNameToFilename(
					Package->GetPathName(), FPackageName::GetAssetPackageExtension());

				UE_LOG(LogClass, Log, TEXT("Saving asset to: %s..."), *PackageName);

				if (Package->SavePackage(Package, AssetInstance, RF_Standalone, *PackageName, GLog))
				{
					UE_LOG(LogClass, Log, TEXT("Done."));
				}
				else
				{
					UE_LOG(LogClass, Warning, TEXT("Can't save asset!"));
				}
			}
		}
	}
}
